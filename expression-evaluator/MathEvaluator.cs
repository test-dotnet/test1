using System.Data;
using System.Text.RegularExpressions;
using CodingSeb.ExpressionEvaluator;

namespace expression_evaluator;
/// <summary>
/// Evaluate math equation
/// </summary>
public partial class MathEvaluator
{
    private static readonly Regex HasAlphabetsFormatRegex = new(@"[a-zA-Z]");
    private static readonly Regex HasSpecialCharFormatRegex = new(@"[!:;?&#_./]"); // Exclude / if you want to validate divide operator.
    
    public MathEvaluator()
    {
    }

    /// <summary>
    /// Execute an evaluation of a math expression
    /// </summary>
    /// <param name="expression">a string math expression</param>
    /// <returns>Tuple of bool result evaluation and his math result operation</returns>
    public static (bool, string?) MathEvaluation(string expression)
    {

        // Confirm if Math expression has no alphabets
        if (HasAlphabetsFormatRegex.Matches(expression).Count != 0)
        {
            throw new InvalidExpressionException("Your expression can't contains Alphabets.");
        }

        // Confirm if Math expression has no special char
        if (HasSpecialCharFormatRegex.Matches(expression).Count != 0)
        {
            throw new InvalidExpressionException("Your expression must be Interger and can't contains special Char.");
        }

        ExpressionEvaluator evaluator = new ExpressionEvaluator();
        return (true, evaluator.Evaluate(expression).ToString());
    }
}

Test 1 :

The developer must code in C# an expression evaluator (including unit tests) which can evaluate the following expression:
•	1 + 2
•	3 * 4
•	11 - 2
•	2 * 3 – 1
•	6 – 2 * 5
The developer can consider that tokens are separated with a space.
Only the 3 operators have to be implemented: plus (+), minus (-) and multiply (*)
The code have to be extensible if we need to add a new operator like divide (/)
The expression handle only integers (no floating decimal).
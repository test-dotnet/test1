﻿using System.Data;

namespace expression_evaluator;

class Program
{
    static void Main(string[] args)
    {
        // Define list of expression
        string[] formulas = new string[] {
            "1 + 2",
            "3 * 4",
            "11 - 2",
            "2 * 3 - 1",
            "6 - 2 * 5"
        };

        // Make evaluation
        foreach (string expression in formulas)
        {
            dynamic result = (false, "0");

            try
            {
                result = MathEvaluator.MathEvaluation(expression);
            }
            catch (InvalidExpressionException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (InvalidDataException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (result.Item1)
                {
                    Console.WriteLine("{0} = {1}", expression, result.Item2);
                }
                else
                {
                    Console.WriteLine("Expression {0} isn't correct", expression);
                }
            }
        }

        Console.ReadKey();
    }
}

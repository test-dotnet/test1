# ExpressionEvaluator Project

## About the project

Project based from the first subject in the .NET Test.

The developer must code in C# an expression evaluator (including unit tests) which can evaluate the following expression:

    1 + 2
    3 * 4
    11 - 2
    2 * 3 – 1
    6 – 2 * 5

The developer can consider that tokens are separated with a space.
Only the 3 operators have to be implemented: plus (+), minus (-) and multiply (*)

The code have to be extensible if we need to add a new operator like divide (/)
The expression handle only integers (no floating decimal).

### Built With

![.NET][dotnet]
 

### Tools
- Visual Studio 2022 and .NET 7.0
- Visual Studio Code 1.8x
- Git


### Features
* Console based programme using these Stack:
- ExpressionEvaluator (Nugets)
- Unit Testing (xUnit Project)


## Prerequisites
* .net 7.0 LTS ( SDK & Runtime)
* Windows ou Linux based system


## Installation
1. Clone or download the repository
```sh
git clone git@gitlab.com:test-dotnet/test1.git
cd test1
cd [expression-evaluator]
dotnet retore
```

2. Run Console Application
```sh
dotnet run
```
Or run it on Microsoft Visual Studio

3. Run xUnit test Application
```sh
dotnet test
```
Or run it on Microsoft Visual Studio

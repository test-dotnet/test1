using expression_evaluator;
using System.Data;


namespace ExpressionEvaluator.Test
{
    public class ExpressionEvaluatorUnitTest
    {

        [Fact]
        public void Expression_Evaluated_Contains_Alphabets_ThrowsInvalidExpressionException()
        {
            string equation = "1 +a";
            try
            {
                MathEvaluator.MathEvaluation(equation);
            }
            catch (Exception e)
            {
                Assert.NotNull(e);
                Assert.IsType<InvalidExpressionException>(e);
                Assert.Equal("Your expression can't contains Alphabets.", e.Message);
                System.Diagnostics.Trace.WriteLine("Expression_Evaluated_Contains_Alphabets_ThrowsInvalidExpressionException");
            }
        }

        [Fact]
        public void Expression_Evaluated_Contains_Speciale_Char_ThrowsInvalidExpressionException()
        {
            string equation = "1 +!2";
            try
            {
                MathEvaluator.MathEvaluation(equation);
            } catch(Exception e) {
                Assert.NotNull(e);
                Assert.IsType<InvalidExpressionException>(e);
                Assert.Equal("Your expression must be Interger and can't contains special Char.", e.Message);
                System.Diagnostics.Trace.WriteLine("Expression_Evaluated_Contains_Speciale_Char_ThrowsInvalidExpressionException");
            }
        }

        [Fact]
        public void Expression_Evaluated_Contains_Decimal_ThrowsInvalidExpressionException()
        {
            string equation = "1.1 + 2";
            try
            {
                MathEvaluator.MathEvaluation(equation);
            }
            catch (Exception e)
            {
                Assert.NotNull(e);
                Assert.IsType<InvalidExpressionException>(e);
                Assert.Equal("Your expression must be Interger and can't contains special Char.", e.Message);
                System.Diagnostics.Trace.WriteLine("Expression_Evaluated_Contains_Decimal_ThrowsInvalidExpressionException");
            }
        }

        [Fact]
        public void Expression_Whith_Space_Can_Be_Evaluated()
        {
            string equation = "1 + 2";
            dynamic result = MathEvaluator.MathEvaluation(equation);
            Assert.NotNull(result);
            Assert.NotNull(result.Item1);
            Assert.NotNull(result.Item2);
            Assert.True(result.Item1);
            System.Diagnostics.Trace.WriteLine("Expression_Whith_Space_Can_Be_Evaluated");
        }

        [Fact]
        public void Expression_Whithout_Space_Can_Be_Evaluated()
        {
            string equation = "1+2";
            dynamic result = MathEvaluator.MathEvaluation(equation);
            Assert.NotNull(result);
            Assert.NotNull(result.Item1);
            Assert.NotNull(result.Item2);
            Assert.True(result.Item1);
            System.Diagnostics.Trace.WriteLine("Expression_Whithout_Space_Can_Be_Evaluated");
        }

        [Fact]
        public void A_Simple_Correct_Expression_Can_Be_Evaluated_And_Must_Have_A_Correct_Result_Operation()
        {
            string equation = "1 + 2";
            dynamic result = MathEvaluator.MathEvaluation(equation);
            Assert.NotNull(result);
            Assert.NotNull(result.Item1);
            Assert.NotNull(result.Item2);
            Assert.True(result.Item1);
            Assert.Equal("3", result.Item2);
            System.Diagnostics.Trace.WriteLine("A_Simple_Correct_Expression_Can_Be_Evaluated_And_Must_Have_A_Correct_Result_Operation");
        }

        [Fact]
        public void Addition_Expression_Evaluated_Must_Return_Addition_Result()
        {
            string equation = "4 + 2";
            dynamic result = MathEvaluator.MathEvaluation(equation);
            Assert.NotNull(result);
            Assert.NotNull(result.Item1);
            Assert.NotNull(result.Item2);
            Assert.True(result.Item1);
            Assert.Equal("6", result.Item2);
            System.Diagnostics.Trace.WriteLine("Addition_Expression_Evaluated_Must_Return_Addition_Result");
        }

        [Fact]
        public void Substraction_Expression_Evaluated_Must_Return_Substraction_Result()
        {
            string equation = "4 - 2";
            dynamic result = MathEvaluator.MathEvaluation(equation);
            Assert.NotNull(result);
            Assert.NotNull(result.Item1);
            Assert.NotNull(result.Item2);
            Assert.True(result.Item1);
            Assert.Equal("2", result.Item2);
            System.Diagnostics.Trace.WriteLine("Substraction_Expression_Evaluated_Must_Return_Substraction_Result");
        }

        [Fact]
        public void Multiplication_Expression_Evaluated_Must_Return_Multiplication_Result()
        {
            string equation = "4 * 2";
            dynamic result = MathEvaluator.MathEvaluation(equation);
            Assert.NotNull(result);
            Assert.NotNull(result.Item1);
            Assert.NotNull(result.Item2);
            Assert.True(result.Item1);
            Assert.Equal("8", result.Item2);
            System.Diagnostics.Trace.WriteLine("Multiplication_Expression_Evaluated_Must_Return_Multiplication_Result");
        }

        [Fact]
        public void Divise_Expression_Evaluated_ThrowsInvalidExpressionException()
        {
            string equation = "4 / 2";
            try
            {
                MathEvaluator.MathEvaluation(equation);
            }
            catch (Exception e)
            {
                Assert.NotNull(e);
                Assert.IsType<InvalidExpressionException>(e);
                Assert.Equal("Your expression must be Interger and can't contains special Char.", e.Message);
                System.Diagnostics.Trace.WriteLine("Divise_Expression_Evaluated_ThrowsInvalidExpressionException");
            }
        }

        [Fact]
        public void Correct_Complicated_Expression_Evaluated_Must_Return_Correct_Result()
        {
            string equation = "2 + 2 * 4";
            dynamic result = MathEvaluator.MathEvaluation(equation);
            Assert.NotNull(result);
            Assert.NotNull(result.Item1);
            Assert.NotNull(result.Item2);
            Assert.True(result.Item1);
            Assert.Equal("10", result.Item2);
            System.Diagnostics.Trace.WriteLine("Correct_Complicated_Expression_Evaluated_Must_Return_Correct_Result");
        }
    }
}